<?php

namespace App\Services;

use App\Repositories\User\UserRepository;

class UserService
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function find($id, $columns = ['*'])
    {
        return $this->userRepository->find($id, $columns);
    }
}
