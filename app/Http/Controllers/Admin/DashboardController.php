<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.dashboard');
    }

    public function get(Request $request)
    {
        $response = Http::get('https://echarts.apache.org/examples/data/asset/data/life-expectancy-table.json', [
        ]);
        return response()->json($response->json(), 200);
    }
}
