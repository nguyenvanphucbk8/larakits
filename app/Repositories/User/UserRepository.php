<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseModelRepository;

class UserRepository extends BaseModelRepository implements UserRepositoryInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function model()
    {
        return User::class;
    }
}
