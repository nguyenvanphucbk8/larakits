<?php

namespace App\Repositories;

interface RepositoryModelInterface
{
    public function getAll($columns = ['*'], $limit = null);

    public function find($id, $columns = ['*']);

    public function create($attributes = []);

    public function update($id, $attributes = []);

    public function delete($id);
}
