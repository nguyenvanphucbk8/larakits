<?php

namespace App\Repositories;

abstract class BaseModelRepository implements RepositoryModelInterface
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    // Start get Model
    abstract public function model();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->model()
        );
    }

    /**
     * Find resource.
     * 
     * @param array $columns
     * @param mixed $limit
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getAll($columns = ['*'], $limit = null)
    {
        return $this->model->take($limit)->get($limit);
    }

    /**
     * Find resource.
     * 
     * @param mixed $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * Create new resource.
     * 
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    /**
     * Create new resource.
     * 
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function insert($attributes = [])
    {
        return $this->model->insert($attributes);
    }

    /**
     * Update existing resource.
     * 
     * @param mixed $id
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, $attributes = [])
    {
        return $this->model->where('id', $id)->update($attributes);
    }

    /**
     * Delete existing resource.
     * 
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->model->delete($id) ? true : false;
    }
}
