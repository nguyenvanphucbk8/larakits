<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    protected $table;

    public function __construct()
    {
        $this->setTable();
    }

    // Start get Table
    abstract public function table();

    public function setTable()
    {
        $this->table = DB::table($this->table());
    }

    public function getAll($columns = ['*'], $limit = null)
    {
        return $this->table->take($limit)->get($limit);
    }

    public function find($id, $columns = ['*'])
    {
        return $this->table->find($id, $columns);
    }

    public function create($attributes = [])
    {
        return $this->table->insert($attributes);
    }

    public function update($id, $attributes = [])
    {
        return $this->table
            ->where('id', $id)
            ->update($attributes);
    }

    public function delete($id)
    {
        return $this->table->where('id', $id)->delete() ? true : false;
    }
}
