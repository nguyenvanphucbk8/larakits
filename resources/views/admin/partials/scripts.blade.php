{{-- Bootstrap 5 JS --}}
<script src="{{ asset('admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js?version_app=' . config('app.version_app')) }}"></script>

<!-- Vendor JS Files -->
<script src="{{ asset('admin/assets/vendor/apexcharts/apexcharts.min.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/vendor/chart.js/chart.umd.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/vendor/echarts/echarts.min.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/vendor/quill/quill.min.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/vendor/simple-datatables/simple-datatables.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/vendor/tinymce/tinymce.min.js?version_app=' . config('app.version_app')) }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('admin/assets/js/main.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/js/select2.min.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/js/sweetalert2@11.js?version_app=' . config('app.version_app')) }}"></script>
