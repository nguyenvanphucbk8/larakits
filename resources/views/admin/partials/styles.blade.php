<!-- Fonts -->
<link href="https://fonts.bunny.net" rel="preconnect">
<link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet">

<!-- Favicons -->
<link href="{{ asset('admin/assets/img/favicon.png?version_app=' . config('app.version_app')) }}" 
    rel="icon">
<link href="{{ asset('admin/assets/img/apple-touch-icon.png?version_app=' . config('app.version_app')) }}"
    rel="apple-touch-icon">

<!-- Google Fonts -->
<link href="https://fonts.gstatic.com" rel="preconnect">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="{{ asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/bootstrap-icons/bootstrap-icons.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/boxicons/css/boxicons.min.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/quill/quill.snow.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/quill/quill.bubble.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/remixicon/remixicon.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">
<link href="{{ asset('admin/assets/vendor/simple-datatables/style.css?version_app=' . config('app.version_app')) }}"
    rel="stylesheet">

<!-- Template Main CSS File -->
<link href="{{ asset('admin/assets/css/style.css?version_app=' . config('app.version_app')) }}" rel="stylesheet">
<!-- =======================================================
* Template Name: NiceAdmin - v2.5.0
* Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
======================================================== -->

<!-- Optional CSS -->
<link href="{{ asset('admin/assets/css/select2.min.css?version_app=' . config('app.version_app')) }}" rel="stylesheet">

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('admin/assets/js/jquery-3.5.1.min.js?version_app=' . config('app.version_app')) }}"></script>
<script src="{{ asset('admin/assets/js/popper.1.16.1.min.js?version_app=' . config('app.version_app')) }}"></script>
