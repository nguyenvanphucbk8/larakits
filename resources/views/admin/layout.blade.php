<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    @stack('head')

    <!-- ======= End: styles ======= -->
    @include('admin.partials.styles')
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @stack('styles')
    <!-- ======= End: styles ======= -->

    <style>
        .collapse.show {
            visibility: initial;
        }
    </style>

</head>

<body>

    <!-- ======= Begin: header ======= -->
    @include('admin.partials.header')
    <!-- ======= End: header ======= -->


    <!-- ======= Begin: sidebar ======= -->
    @include('admin.partials.sidebar')
    <!-- ======= End: sidebar ======= -->


    <!-- ======= Begin: content ======= -->
    @yield('content')
    <!-- ======= End: content ======= -->


    <!-- ======= Begin: footer ======= -->
    @include('admin.partials.footer')
    <!-- ======= End: footer ======= -->


    <!-- ======= Begin: modal ======= -->
    @stack('modal')
    <!-- ======= End: modal ======= -->


    <!-- ======= End: scripts ======= -->
    @include('admin.partials.scripts')
    @stack('scripts')
    <!-- ======= End: scripts ======= -->

</body>

</html>
