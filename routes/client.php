<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Client Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', [App\Http\Controllers\Client\HomeController::class, 'index'])->name('home.page.index');
